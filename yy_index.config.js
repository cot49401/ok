var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// src/index.config.js
var index_config_exports = {};
__export(index_config_exports, {
  default: () => index_config_default
});
module.exports = __toCommonJS(index_config_exports);
var index_config_default = {
  kunyu77: {
    testcfg: {
      bbbb: "aaaaa"
    }
  },
  wogg: {
    url: "https://www.wogg.link/"
  },
  //baipiaoys: {
  // url: "https://www.baipiaoys.com:9092",
  // jx: "https://jscdn.centos.chat/bilfun.php/?url="
  //},
  youtube: {
    url: "搭建小雅alist-tvbox的服务器ip:4567/youtube"
  },
  ali: {
    token: "d14192231cb84de0a8611d82a0de8b20",
    token280: "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIxMGExYTI4ODNmNGE0YWMzOTUzNjkyOWJmMTEwOWMwOSIsImF1ZCI6Ijc2OTE3Y2NjY2Q0NDQxYzM5NDU3YTA0ZjYwODRmYjJmIiwiZXhwIjoxNzE5Mjg5MDUyLCJpYXQiOjE3MTE1MTMwNTIsImp0aSI6ImFlNzM1MzYzYzhkMjQ0YzE4MWEzNjAzZTA1ZTk0Y2JlIn0.dfbT1IJgh6TzndhOw4MFlis84Pb9R-vHafAXZbJ3gUJ8zoHusDoF3Zkx-sV0gaI3GPggbAO02LymM4t-x87-rQ"
  },
  quark: {
    cookie: "_UP_A4A_11_=wb96311314894855ba152f92ddf572de; _UP_D_=pc; _UP_30C_6A_=st96362019cglxcehlgc5kruvyn65tun; _UP_TS_=sg196b4d8c24cb11ea3df080c1455321b62; _UP_E37_B7_=sg196b4d8c24cb11ea3df080c1455321b62; _UP_TG_=st96362019cglxcehlgc5kruvyn65tun; _UP_335_2B_=1; __pus=5d8567909c6f7c622969e5990f663461AAS4841n3XjDZxnvJsoMLL8GqW/gdIyhxKEhfatZX8hcflKmvVKALYhHP+RFw7I1Sl0SxjKqmBpSgfyi+YfezgB5; __kp=77737580-e805-11ee-8aa0-3d3557f8aafc; __kps=AAQPKaBYCBG2d38WWSVGixMf; __ktd=FnseRlKW2WF0kI+HUSHWsg==; __uid=AAQPKaBYCBG2d38WWSVGixMf; tfstk=f5Is1RDEvfc_TCLtzteFFSff69xbU1ZzCx9AEtnZHhKtDs6DTduaSEPfRsCe6GzM_6OfUtm2_O-Ths1R1E5fnRvvMsCXgNzU4OXMmnFyGurPIIqbDR16WEBLHK9m6KjDWkWMmnFUsg5X7OfrdtiMGneBvK9qknL9WJOpnBn9Hcpx9J9DP3orJ7UNIkiibmbCUV-8X6fFAdTL8TALIgI6CFORBGixDzp65QB9O7_Oq3LdnEsmnmtlBafDe1hTaFXAR696cku929_ANpjT4f9h79By9TMmmOx6hUKvRxnAdhApyGx82X9hJO7Aj1MjDdjN4EOkR-nDuHIyyaCsn8XpXKIr80RW4Q0jdUmvdQyQdq0mq5ixis0V3uY9KLAzdJgPnbmxmL2QdXgDWpv-UJwIztf..; __puus=300a3af7bc5dac4747df6fbfc681de60AASmjDnD4h/iXZxzYBQZypA6L/ohiAcN3QUq+QeoP6+O9N7JDUcr89m6zdrrnJ4UfU9gMl7R9/Scff875KF7L4M+aOUuw/jofKFP4SRYFc5at6Esw7Q1yn9S94tlLYD3d+jA++XQSx0lhymfxwR2rLwA5H2RuI09QCA9gT037qYZ5sh0KBeki9XzQKJMuMqQz6fG3lV8AFG+8056Gqc8dL4O"
  },
  tudou: {
    url: "https://tudou.lvdoui.top"
  },
  wobg: {
    url: "https://wobge.run.goorm.io"
  },
  cm: {
    url: "https://tv.yydsys.top"
  },
  nangua: {
    url: "http://ys.changmengyun.com"
  },
  ysche: {
    url: "https://www.weixine.link"
  },
  xiaoya: {
    url: "http://120.76.118.109:4567/vod1/"
  },
  nm: {
    url: "https://v.nmvod.cn"
  },
  unyun: {
    url: "https://zyb.upyunso.com"
  },
  jojo: {
    name: "JOJO影视",
    url: "https://jiohub.top"
  },
  xxys: {
    url: "http://yingszj.xn--654a.cc/api.php/app/"
  },
  live: {
    contents: "https://gitee.com/galnt/cctv/raw/master/contents.txt",
    // 按省区分的目录,供参考,可以不添加
    url: [
      {name: "优质源",url:"https://mirror.ghproxy.com/https://raw.githubusercontent.com/wwb521/live/main/tv.txt",index: "true"},
      {name:"live",url:"http://home.jundie.top:81/Cat/tv/live.txt",index: "false"},
      {name: "范明明",url:"https://live.fanmingming.com/tv/m3u/ipv6.m3u",index: "false"},
      {name:"18+🚷","url":"https://mirror.ghproxy.com/https://raw.githubusercontent.com/ctsfork/web/main/iptv/hd-sex-all.txt",index: "true"},
      {name:"TVLIVE",url:"https://tvkj.top/tvlive.txt",index: "true"},
      {name:"pl",url:"https://raw.gitcode.com/tytv/tydz/raw/main/pl.txt",index: "true"},
      {name: "道长源","url":"https://mirror.ghproxy.com/raw.githubusercontent.com/dxawi/0/main/tvlive.txt"},
      {name: "俊于源","url":"http://home.jundie.top:81/Cat/tv/live.txt"},
      {name: "国际",url:"https://fanmingming.com/txt?url=https://mirror.ghproxy.com/https://raw.githubusercontent.com/YueChan/Live/main/IPTV.m3u",index: "true"}
    ]
  },
  libvio: {
    url: "https://libvio.app"
  },
  ttkx: {
    url: "http://ttkx.live:3328"
  },
  czzy: {
    url: "https://cz01.vip"
  },
  bili: {
    categories: "佛教音乐#经典无损音乐合集#帕梅拉#太极拳#健身#舞蹈#音乐#歌曲#MV4K#演唱会4K#白噪音4K#知名UP主#说案#解说#演讲#时事#探索发现超清#纪录片超清#沙雕动画#沙雕穿越#沙雕#平面设计教学#软件教程#实用教程#旅游#风景4K#食谱#美食超清#搞笑#球星#动物世界超清#相声小品#戏曲#儿童#小姐姐4K#热门#旅行探险",
    cookie: "buvid3=0C0EAA18-337F-AAF2-24AE-056923FB837C49297infoc; bsource=search_baidu; _uuid=101C54512-F10CA-4636-B765-4EE8FE39894951470infoc; buvid4=FFAF1612-66B0-CAC0-5CF3-1455817C5C6851611-123090815-uk4%2FG28K78zhW9xeZbPKMgakoJaD5BH70OPlgUluoy%2BvEotkFzXwug%3D%3D; buvid_fp=cd8bc9cd00c18c6d5cd995c568d3d4ac; b_nut=100; rpdid=0zbfVFT5WN|C5GmE9v7|21C|3w1QEBl6; enable_web_push=DISABLE; header_theme_version=CLOSE; home_feed_column=4; is-2022-channel=1; CURRENT_FNVAL=1; innersign=0; browser_resolution=1100-2054; DedeUserID=1958790055; DedeUserID__ckMd5=317ab4d7713cf829; FEED_LIVE_VERSION=V_WATCHLATER_PIP_WINDOW2; b_lsid=722B2E69_18EA1305CAB; bili_ticket=eyJhbGciOiJIUzI1NiIsImtpZCI6InMwMyIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTIzNjA0ODYsImlhdCI6MTcxMjEwMTIyNiwicGx0IjotMX0.SPYe0Btd-dfNU74W_PR3l2Zb72Gt2iJxbC4T4K_qiEg; bili_ticket_expires=1712360426; SESSDATA=f2d9c424%2C1727653427%2C31bff%2A42CjAs9-pj-71jhsqEIWDEyrx3ewz8EwSX4WHCwzbHEqbmFhayvSGnUSjaVs_IXrHdoR4SVmxsamVMc0gwOVRObEFEY1lhdWpSZWJsLUZHdFVlUDVDbFBSek02UUFOZE1OS3UycDVuTi1KMFVEaXVFVW5sazR2Z3ZlSTFNMWozbU04NE1YeVRfM2x3IIEC; bili_jct=d27c21aa284bcb097ebdb448e03cce2c; sid=fajzlcbv"
  },
  ffm3u8: {
    url: "https://cj.ffzyapi.com/api.php/provide/vod/from/ffm3u8/",
    categories: ["国产剧", "香港剧", "韩国剧", "欧美剧", "台湾剧", "日本剧", "海外剧", "泰国剧", "短剧", "动作片", "喜剧片", "爱情片", "科幻片", "恐怖片", "剧情片", "战争片", "动漫片", "大陆综艺", "港台综艺", "日韩综艺", "欧美综艺", "国产动漫", "日韩动漫", "欧美动漫", "港台动漫", "海外动漫", "记录片"]
  },
  yycjm3u8: {
    name: "业余无广",
    url: "https://yyff.540734621.xyz/api.php/provide/vod/",
    categories: ["热播短剧", "国产电影", "国产剧", "动作片", "喜剧片", "爱情片", "剧情片", "科幻片", "战争片", "恐怖片", "欧美剧", "港台剧", "日韩剧", "海外剧", "国内动漫", "日韩动漫", "欧美动漫", "国内综艺", "欧美综艺", "日韩综艺", "纪录片", "音乐", "体育"]
  },
  m3u8cj: {
    ffm3u8: [{
      name: "非凡采集",
      url: "https://cj.ffzyapi.com/api.php/provide/vod/from/ffm3u8/",
      search: true
    }],
    lzm3u8: [{
      name: "量子采集",
      url: "https://cj.lziapi.com/api.php/provide/vod/from/lzm3u8/",
      search: true
    }],
    suoni8: [{
      name: "索尼采集",
      url: "https://suoniapi.com/api.php/provide/vod/from/snm3u8/",
      search: true
    }],
    subm3u: [{
      name: "速播采集",
      url: "https://subocaiji.com/api.php/provide/vod/from/subm3u8/",
      search: true
    }],
    hhm3u8: [{
      name: "豪华采集",
      url: "https://hhzyapi.com/api.php/provide/vod/from/hhm3u8/",
      search: true
    }],
    qhm3u8: [{
      name: "奇虎采集",
      url: "https://caiji.qhzyapi.com/api.php/provide/vod/from/qhm3u8/",
      search: true
    }],
    hnm3u8: [{
      name: "红牛采集",
      url: "https://www.hongniuzy2.com/api.php/provide/vod/from/hnm3u8/",
      search: true
    }],
    xlm3u8: [{
      name: "新浪采集",
      url: "https://api.xinlangapi.com/xinlangapi.php/provide/vod/from/xlm3u8/",
      search: true
    }],
    kuaika: [{
      name: "快看采集",
      url: "https://kuaikan-api.com/api.php/provide/vod/from/kuaikan/",
      search: true
    }],
    wjm3u8: [{
      name: "无尽采集",
      url: "https://api.wujinapi.me/api.php/provide/vod/from/wjm3u8/",
      search: true
    }],
    lsm3u8: [{
      name: "乐视采集",
      url: "https://leshiapi.com/api.php/provide/vod/at/json/",
      search: true
    }],
    jjm3u8: [{
      name: "极速资源",
      url: "https://jszyapi.com/api.php/provide/vod/from/jsm3u8/",
      search: true
    }],
    gjm3u8: [{
      name: "光速资源",
      url: "https://api.guangsuapi.com/api.php/provide/vod/from/gsm3u8/",
      search: true
    }],
    bfzyu8: [{
      name: "暴风采集",
      url: "https://bfzyapi.com/api.php/provide/vod/",
      search: true
    }],
    t1080z: [{
      name: "适度采集",
      url: "https://api.1080zyku.com/inc/apijson.php",
      search: true
    }]
  },
  m3u8av: {
    ykm3u8: [{
      name: "易看资源",
      url: "https://api.yikanapi.com/api.php/provide/vod/from/yikan",
      search: true
    }],
    tianme: [{
      name: "天美资源",
      url: "https://api.yikanapi.com/api.php/provide/vod/",
      search: true
    }],
    tianmq: [{
      name: "美天资源",
      url: "http://www.tianmei.pw/api.php/provide/vod/from/m3u8/",
      search: true
    }],
    clm3u8: [{
      name: "草榴资源",
      url: "https://www.caoliuzyw.com/api.php/provide/vod/from/clm3u8",
      search: true
    }],
    tmm3u8: [{
      name: "甜蜜资源",
      url: "https://timizy10.cc/api.php/provide/vod/from/timim3u8",
      search: true
    }],
    kkm3u8: [{
      name: "写真资源",
      url: "https://kkzy.me/api.php/provide/vod/",
      search: true
    }],
    askm3u: [{
      name: "奥卡资源",
      url: "https://aosikazy.com/api.php/provide/vod/",
      search: true
    }],
    sngm3u: [{
      name: "南国玉兔",
      url: "https://api.sexnguon.com/api.php/provide/vod/",
      search: true
    }],
    ptm3u8: [{
      name: "葡萄资源",
      url: "https://caiji.putaozyw.net/inc/apijson_vod.php",
      search: true
    }],
    xbm3u8: [{
      name: "雪豹资源",
      url: "https://api.xbapi.cc/api.php/provide/vod/",
      search: true
    }],
    sw401m: [{
      name: "丝袜资源",
      url: "https://www.siwazyw.tv/api.php/provide/vod/",
      search: true
    }],
    swf02m: [{
      name: "丝袜资湖",
      url: "https://sewozyapi.com/api.php/provide/vod",
      search: true
    }],
    swh02m: [{
      name: "涩窝资源",
      url: "https://sewozyapi.com/api.php/provide/vod",
      search: true
    }],
    adm3u8: [{
      name: "爱豆资源",
      url: "http://chujia.cc/api.php/provide/vod/from/m3u8",
      search: true
    }],
    ddx1m3: [{
      name: "滴滴资源",
      url: "https://api.ddapi.cc/api.php/provide/vod/",
      search: true
    }],
    ddk2m3: [{
      name: "蛋蛋资源",
      url: "http://156.249.29.8/inc/apijson_vod.php",
      search: true
    }],
    lym3u8: [{
      name: "老鸭资源",
      url: "https://api.apilyzy.com/api.php/provide/vod/",
      search: true
    }],
    yrm3u8: [{
      name: "伊人网源",
      url: "https://api.yrzyapi.com/api.php/provide/vod/",
      search: true
    }],
    yrm3w8: [{
      name: "伊人网源",
      url: "https://api.yirenziyuan.com/api.php/provide/vod/at/json",
      search: true
    }],
    mym3u8: [{
      name: "猫源传媒",
      url: "https://api.maozyapi.com/inc/apijson_vod.php",
      search: true
    }],
    sy1m3u: [{
      name: "鲨鱼资源",
      url: "https://shayuapi.com/api.php/provide/vod/",
      search: true
    }],
    sy2m3u: [{
      name: "色鸭资源",
      url: "https://api.seyavod.com/api.php/provide/vod/",
      search: true
    }],
    hxm3u8: [{
      name: "红袖资源",
      url: "https://hongxiuzy.com/api.php/provide/vod/",
      search: true
    }],
    sbm3u8: [{
      name: "色逼资源",
      url: "https://apilsbzy1.com/api.php/provide/vod/at/json",
      search: true
    }],
    kom3u8: [{
      name: "KOK2资源",
      url: "https://api.vodkok2.com/api.php/provide/vod/at/json",
      search: true
    }],
    jdm3u8: [{
      name: "精东影业",
      url: "http://chujia.cc/api.php/provide/vod/from/m3u8/",
      search: true
    }],
    jym3u8: [{
      name: "九一资源",
      url: "http://madouse.cc/api.php/provide/vod/",
      search: true
    }],
    kam3u8: [{
      name: "快爱资源",
      url: "http://kuaiavzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    jsm3u8: [{
      name: "久色资源",
      url: "https://9szy.net/api.php/provide/vod/at/json",
      search: true
    }],
    apilj6: [{
      name: "辣椒",
      url: "https://apilj.com/api.php/provide/vod/at/json/",
      search: true
    }],
    jikun5: [{
      name: "鸡坤",
      url: "https://sewozyapi.com/api.php/provide/vod/",
      search: true
    }],
    manggu: [{
      name: "忙果",
      url: "https://madoucun.cn/api.php/provide/vod/?ac=xml",
      search: true
    }],
    mifeng: [{
      name: "蜜蜂",
      url: "https://www.beeyao.com/api.php/provide/vod/at/json",
      search: true
    }],
    madous: [{
      name: "果冻",
      url: "http://imadou.cc/api.php/provide/vod/at/json",
      search: true
    }],
    xiaoca: [{
      name: "小草",
      url: "https://www.caoliuzyw.com/api.php/provide/vod/at/json/",
      search: true
    }],
    lajiao: [{
      name: "辣椒",
      url: "https://apilj.com/api.php/provide/vod/at/json/",
      search: true
    }],
    yikana: [{
      name: "易看",
      url: "https://api.yikanapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    liuyou: [{
      name: "六优",
      url: "http://www.6uzy.cc/inc/apijson_vod.php",
      search: true
    }],
    vipmvd: [{
      name: "天堂",
      url: "http://vipmv.cc/api.php/provide/vod/at/json",
      search: true
    }],
    sanjiu: [{
      name: "三九",
      url: "https://www.39kan.com/api.php/provide/vod/at/json",
      search: true
    }],
    piaohu: [{
      name: "飘花",
      url: "http://www.zzrhgg.com/api.php/provide/vod/at/json",
      search: true
    }],
    z49zyw: [{
      name: "四九",
      url: "https://49zyw.com/api.php/provide/vod/at/json",
      search: true
    }],
    z68zy8: [{
      name: "六八",
      url: "https://68zy88.com/api.php/provide/vod/at/json",
      search: true
    }],
    subosd: [{
      name: "速博",
      url: "https://subocaiji.com/api.php/provide/vod/at/json",
      search: true
    }],
    huyade: [{
      name: "虎牙",
      url: "https://www.huyaapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    kuakez: [{
      name: "夸克",
      url: "http://kuakezy1.com/api.php/provide/vod/at/json",
      search: true
    }],
    qhzyap: [{
      name: "奇虎",
      url: "https://caiji.qhzyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    wandou: [{
      name: "豌豆",
      url: "https://ks.wandouzy.cc/api.php/provide/vod/at/json",
      search: true
    }],
    jiguan: [{
      name: "极光",
      url: "https://jiguang.la/api.php/provide/vod/at/json",
      search: true
    }],
    hw8sds: [{
      name: "华为",
      url: "https://hw8.live/api.php/provide/vod/at/json",
      search: true
    }],
    xzcjzs: [{
      name: "熊掌",
      url: "https://xzcjz.com/api.php/provide/vod/at/json",
      search: true
    }],
    kekezy: [{
      name: "可可",
      url: "https://caiji.kekezyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    xkanzy: [{
      name: "享看",
      url: "https://xkanzy10.com/api.php/provide/vod/at/json",
      search: true
    }],
    damozy: [{
      name: "大漠",
      url: "https://damozy.com/api.php/provide/vod/at/json",
      search: true
    }],
    kuaika: [{
      name: "快看",
      url: "https://kuaikan-api.com/api.php/provide/vod/at/json",
      search: true
    }],
    kuaiyu: [{
      name: "快云",
      url: "https://www.kuaiyunzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    jszyap: [{
      name: "极速",
      url: "https://jszyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    hhzyap: [{
      name: "火狐",
      url: "https://hhzyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    ikunzy: [{
      name: "爱坤",
      url: "https://ikunzyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    z424fe: [{
      name: "四二",
      url: "https://www.42.la/api.php/provide/vod/from/42m3u8/at/json",
      search: true
    }],
    kczyap: [{
      name: "快车",
      url: "https://caiji.kczyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    collec: [{
      name: "卧龙",
      url: "https://collect.wolongzyw.com/api.php/provide/vod/at/json",
      search: true
    }],
    m3u8ap: [{
      name: "樱花",
      url: "https://m3u8.apiyhzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    sdzyap: [{
      name: "闪电",
      url: "http://sdzyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    hanjuz: [{
      name: "韩剧",
      url: "http://www.hanjuzy.com/inc/apijson_vod.php",
      search: true
    }],
    cjbaji: [{
      name: "八戒",
      url: "http://cj.bajiecaiji.com/inc/apijson_vod.php",
      search: true
    }],
    apiapi: [{
      name: "百度",
      url: "https://api.apibdzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    apiwuj: [{
      name: "无尽",
      url: "https://api.wujinapi.net/api.php/provide/vod/at/json",
      search: true
    }],
    apitia: [{
      name: "天空",
      url: "https://api.tiankongapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    pgfenw: [{
      name: "四圈",
      url: "https://pg.fenwe078.cf/api.php/provide/vod/at/json",
      search: true
    }],
    rrvipw: [{
      name: "人人",
      url: "https://www.rrvipw.com/api.php/provide/vod/at/json",
      search: true
    }],
    haowyw: [{
      name: "小小",
      url: "http://haowywz.com/api.php/provide/vod/at/json",
      search: true
    }],
    cjvodi: [{
      name: "影图",
      url: "https://cj.vodimg.top/api.php/provide/vod/at/json",
      search: true
    }],
    p2100d: [{
      name: "飘零",
      url: "https://p2100.net/api.php/provide/vod/at/json",
      search: true
    }],
    movieg: [{
      name: "新马",
      url: "https://movie.gsl99.com/api.php/provide/vod/at/json",
      search: true
    }],
    feifei: [{
      name: "飞飞",
      url: "http://www.feifei67.com/api.php/provide/vod/at/json",
      search: true
    }],
    zy018d: [{
      name: "十八",
      url: "https://www.zy018.com/api.php/provide/vod/at/json",
      search: true
    }],
    z9szys: [{
      name: "九色",
      url: "https://9szy.net/api.php/provide/vod/at/json",
      search: true
    }],
    apibuk: [{
      name: "不卡",
      url: "https://api.bukazyw.fun/api.php/provide/vod/at/json",
      search: true
    }],
    kuaiav: [{
      name: "快爱",
      url: "http://kuaiavzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    apivod: [{
      name: "KOK",
      url: "https://api.vodkok2.com/api.php/provide/vod/at/json",
      search: true
    }],
    maozys: [{
      name: "猫咪",
      url: "https://api.maozyapi.com/inc/apijson_vod.php",
      search: true
    }],
    siwazy: [{
      name: "袜丝",
      url: "https://www.siwazyw.tv/api.php/provide/vod/at/json",
      search: true
    }],
    siwasd: [{
      name: "丝袜",
      url: "https://haiwai.siwazyw.org/api.php/provide/vod/at/json",
      search: true
    }],
    shayua: [{
      name: "鲨鱼",
      url: "https://shayuapi.com/api.php/provide/vod/",
      search: true
    }],
    apilyz: [{
      name: "老鸭",
      url: "https://api.apilyzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    sewozy: [{
      name: "涩窝",
      url: "https://sewozyapi.com/api.php/provide/vod",
      search: true
    }],
    semaod: [{
      name: "色猫",
      url: "https://caiji.semaozy.net/inc/apijson_vod.php",
      search: true
    }],
    z19821: [{
      name: "花色",
      url: "http://198.211.49.156/api.php/provide/vod/at/json",
      search: true
    }],
    putaoz: [{
      name: "葡萄",
      url: "https://caiji.putaozyw.net/inc/apijson_vod.php",
      search: true
    }],
    apidda: [{
      name: "滴滴",
      url: "https://api.ddapi.cc/api.php/provide/vod/at/json",
      search: true
    }],
    apixba: [{
      name: "️雪豹",
      url: "https://api.xbapi.cc/api.php/provide/vod/at/json",
      search: true
    }],
    apiyir: [{
      name: "伊人",
      url: "https://api.yirenziyuan.com/api.php/provide/vod/at/json",
      search: true
    }],
    kaiyun: [{
      name: "开云",
      url: "https://kaiyunzy5.com/inc/apijson_vod.php",
      search: true
    }],
    seyavo: [{
      name: "色鸭",
      url: "https://api.seyavod.com/api.php/provide/vod/at/json",
      search: true
    }],
    jizhia: [{
      name: "极致",
      url: "https://jizhiapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    timizy: [{
      name: "甜蜜",
      url: "https://timizy10.cc/api.php/provide/vod/at/json",
      search: true
    }],
    aosika: [{
      name: "奥斯",
      url: "https://aosikazy.com/api.php/provide/vod/?ac=list",
      search: true
    }],
    sexngu: [{
      name: "色南",
      url: "https://api.sexnguon.com/api.php/provide/vod/at/json",
      search: true
    }],
    z15624: [{
      name: "蛋蛋",
      url: "http://156.249.29.8/inc/apijson_vod.php",
      search: true
    }],
    kkzydd: [{
      name: "写真",
      url: "https://kkzy.me/api.php/provide/vod/at/json",
      search: true
    }],
    madoud: [{
      name: "麻豆",
      url: "http://wuxu.cc/api.php/provide/vod/from/mdm3u8/",
      search: true
    }],
    tmyysd: [{
      name: "天美",
      url: "https://tmyy.cc/api.php/provide/vod/from/m3u8/",
      search: true
    }],
    apig14: [{
      name: "萝莉",
      url: "https://api.g14o.cc/api.php/provide/vod/at/json",
      search: true
    }],
    bominz: [{
      name: "博民",
      url: "https://www.bominzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    z155ap: [{
      name: "十五",
      url: "https://155api.com/api.php/provide/vod/?ac=list",
      search: true
    }],
    avre00: [{
      name: "黄瓜",
      url: "https://www.avre00.com/api.php/provide/vod/?ac=list",
      search: true
    }],
    dadoud: [{
      name: "大豆",
      url: "https://www.dadou.cm/api.php/provide/vod/at/json",
      search: true
    }],
    ysmd21: [{
      name: "明帝",
      url: "https://ys.md214.cn/api.php/provide/vod/at/json",
      search: true
    }],
    seacms: [{
      name: "花旗",
      url: "https://seacms.huaqi.live/zyapi.php",
      search: true
    }],
    z82156: [{
      name: "至圣",
      url: "http://82.156.40.118:1234/api.php/provide/vod/at/json",
      search: true
    }],
    naixxz: [{
      name: "奶香",
      url: "https://naixxzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    moduzy: [{
      name: "魔都",
      url: "https://www.moduzy.com/api.php/provide/vod/?ac=list",
      search: true
    }],
    taopia: [{
      name: "淘片",
      url: "https://taopianapi.com/cjapi/mc10/vod/json.html",
      search: true
    }],
    apizui: [{
      name: "最大",
      url: "https://api.zuidapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    inmsdf: [{
      name: "映迷",
      url: "https://www.inmi.app/api.php/provide/vod/at/json",
      search: true
    }],
    mgzyz1: [{
      name: "芒果",
      url: "https://mgzyz1.com/api.php/provide/vod/at/json",
      search: true
    }],
    mtasdv: [{
      name: "桃桃",
      url: "https://mtav.art/api.php/provide/vod/at/json",
      search: true
    }],
    apittz: [{
      name: "天天",
      url: "https://apittzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    z8days: [{
      name: "七天",
      url: "https://8day.icu/api.php/provide/vod/at/json",
      search: true
    }],
    z5bod1: [{
      name: "五播",
      url: "https://5bo1.xyz/api.php/provide/vod/at/json",
      search: true
    }],
    z91avd: [{
      name: "九一",
      url: "https://91av.cyou/api.php/provide/vod/at/json",
      search: true
    }],
    z92fre: [{
      name: "久爱",
      url: "http://92free.icu/api.php/provide/vod/at/json",
      search: true
    }],
    tmavsd: [{
      name: "美亚",
      url: "https://tmav.art/api.php/provide/vod/at/json",
      search: true
    }],
    sezyde: [{
      name: "色网",
      url: "https://sezy.website/api.php/provide/vod/at/json",
      search: true
    }],
    xxavse: [{
      name: "湿园",
      url: "https://xxavs.com/api.php/provide/vod/at/json",
      search: true
    }],
    qqcmer: [{
      name: "传媒",
      url: "https://qqcm.sbs/api.php/provide/vod/at/json",
      search: true
    }],
    auezye: [{
      name: "优异",
      url: "https://a.uezy.pw/api.php/provide/vod/at/json",
      search: true
    }],
    hongxi: [{
      name: "红袖",
      url: "https://hongxiuzy.com/api.php/provide/vod/",
      search: true
    }],
    apilsb: [{
      name: "色逼",
      url: "https://apilsbzy1.com/api.php/provide/vod/at/json",
      search: true
    }],
    totose: [{
      name: "橘猫",
      url: "https://to.to-long.com/api.php/provide/vod/at/json",
      search: true
    }],
    huangp: [{
      name: "黄片",
      url: "https://www.pgxdy.com/api/json.php",
      search: true
    }],
    huakui: [{
      name: "花魁",
      url: "https://caiji.huakuiapi.com/inc/apijson_vod.php",
      search: true
    }],
    hghhhd: [{
      name: "皇冠",
      url: "https://hghhh.com/api.php/provide/vod/at/json",
      search: true
    }],
    slapib: [{
      name: "森林",
      url: "https://slapibf.com/api.php/provide/vod/at/json",
      search: true
    }],
    apiyut: [{
      name: "玉兔",
      url: "https://apiyutu.com/api.php/provide/vod/at/json",
      search: true
    }],
    hszsdy: [{
      name: "黄色",
      url: "http://hszy.me/api.php/provide/vod/at/json",
      search: true
    }],
    haopia: [{
      name: "好片",
      url: "https://haopianapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    ziyuan: [{
      name: "速看",
      url: "https://ziyuan.skm3u8.com/api.php/provide/vod/at/json",
      search: true
    }],
    jkunzy: [{
      name: "鸡坤",
      url: "https://jkunzyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    z10042: [{
      name: "零爱",
      url: "http://100.42.227.80/api/macs.php",
      search: true
    }],
    z888da: [{
      name: "抖阴",
      url: "https://www.888dav.com/api.php/provide/vod/at/json",
      search: true
    }],
    z91mdd: [{
      name: "九麻",
      url: "https://91md.me/api.php/provide/vod/at/json",
      search: true
    }],
    msniid: [{
      name: "美女",
      url: "https://www.msnii.com/api/json.php",
      search: true
    }],
    xrbspd: [{
      name: "淫水",
      url: "https://www.xrbsp.com/api/json.php",
      search: true
    }],
    gdlsps: [{
      name: "香奶",
      url: "https://www.gdlsp.com/api/json.php",
      search: true
    }],
    kxgavd: [{
      name: "白嫖",
      url: "https://www.kxgav.com/api/json.php",
      search: true
    }],
    afasud: [{
      name: "湿妹",
      url: "https://www.afasu.com/api/json.php",
      search: true
    }],
    dadiap: [{
      name: "大地",
      url: "https://dadiapi.com/api.php",
      search: true
    }],
    lbapi9: [{
      name: "乐播",
      url: "https://lbapi9.com/api.php/provide/vod/at/json",
      search: true
    }],
    fhapi9: [{
      name: "番号",
      url: "http://fhapi9.com/api.php/provide/vod/at/json",
      search: true
    }]
  },
  u8av: {
    clm3u8: [{
      name: "草榴资源",
      url: "https://www.caoliuzyw.com/api.php/provide/vod/from/clm3u8",
      search: true
    }],
    tmm3u8: [{
      name: "甜蜜资源",
      url: "https://timizy10.cc/api.php/provide/vod/from/timim3u8",
      search: true
    }],
    kkm3u8: [{
      name: "写真资源",
      url: "https://kkzy.me/api.php/provide/vod/",
      search: true
    }],
    askm3u: [{
      name: "奥卡资源",
      url: "https://aosikazy.com/api.php/provide/vod/",
      search: true
    }],
    ptm3u8: [{
      name: "葡萄资源",
      url: "https://caiji.putaozyw.net/inc/apijson_vod.php",
      search: true
    }],
    xbm3u8: [{
      name: "雪豹资源",
      url: "https://api.xbapi.cc/api.php/provide/vod/",
      search: true
    }],
    sw401m: [{
      name: "丝袜资源",
      url: "https://www.siwazyw.tv/api.php/provide/vod/",
      search: true
    }],
    adm3u8: [{
      name: "爱豆资源",
      url: "http://chujia.cc/api.php/provide/vod/from/m3u8",
      search: true
    }],
    ddx1m3: [{
      name: "滴滴资源",
      url: "https://api.ddapi.cc/api.php/provide/vod/",
      search: true
    }],
    lym3u8: [{
      name: "老鸭资源",
      url: "https://api.apilyzy.com/api.php/provide/vod/",
      search: true
    }],
    mym3u8: [{
      name: "猫源传媒",
      url: "https://api.maozyapi.com/inc/apijson_vod.php",
      search: true
    }],
    sy1m3u: [{
      name: "鲨鱼资源",
      url: "https://shayuapi.com/api.php/provide/vod/",
      search: true
    }],
    sbm3u8: [{
      name: "色逼资源",
      url: "https://apilsbzy1.com/api.php/provide/vod/at/json",
      search: true
    }],
    jdm3u8: [{
      name: "精东影业",
      url: "http://chujia.cc/api.php/provide/vod/from/m3u8/",
      search: true
    }],
    kam3u8: [{
      name: "快爱资源",
      url: "http://kuaiavzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    apilj6: [{
      name: "辣椒",
      url: "https://apilj.com/api.php/provide/vod/at/json/",
      search: true
    }],
    xiaoca: [{
      name: "小草",
      url: "https://www.caoliuzyw.com/api.php/provide/vod/at/json/",
      search: true
    }],
    lajiao: [{
      name: "辣椒",
      url: "https://apilj.com/api.php/provide/vod/at/json/",
      search: true
    }],
    sanjiu: [{
      name: "三九",
      url: "https://www.39kan.com/api.php/provide/vod/at/json",
      search: true
    }],
    z49zyw: [{
      name: "四九",
      url: "https://49zyw.com/api.php/provide/vod/at/json",
      search: true
    }],
    huyade: [{
      name: "虎牙",
      url: "https://www.huyaapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    qhzyap: [{
      name: "奇虎",
      url: "https://caiji.qhzyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    hw8sds: [{
      name: "华为",
      url: "https://hw8.live/api.php/provide/vod/at/json",
      search: true
    }],
    kuaika: [{
      name: "快看",
      url: "https://kuaikan-api.com/api.php/provide/vod/at/json",
      search: true
    }],
    kuaiyu: [{
      name: "快云",
      url: "https://www.kuaiyunzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    jszyap: [{
      name: "极速",
      url: "https://jszyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    hhzyap: [{
      name: "火狐",
      url: "https://hhzyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    ikunzy: [{
      name: "爱坤",
      url: "https://ikunzyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    kczyap: [{
      name: "快车",
      url: "https://caiji.kczyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    collec: [{
      name: "卧龙",
      url: "https://collect.wolongzyw.com/api.php/provide/vod/at/json",
      search: true
    }],
    m3u8ap: [{
      name: "樱花",
      url: "https://m3u8.apiyhzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    sdzyap: [{
      name: "闪电",
      url: "http://sdzyapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    cjbaji: [{
      name: "八戒",
      url: "http://cj.bajiecaiji.com/inc/apijson_vod.php",
      search: true
    }],
    apiapi: [{
      name: "百度",
      url: "https://api.apibdzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    apiwuj: [{
      name: "无尽",
      url: "https://api.wujinapi.net/api.php/provide/vod/at/json",
      search: true
    }],
    apitia: [{
      name: "天空",
      url: "https://api.tiankongapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    cjvodi: [{
      name: "影图",
      url: "https://cj.vodimg.top/api.php/provide/vod/at/json",
      search: true
    }],
    p2100d: [{
      name: "飘零",
      url: "https://p2100.net/api.php/provide/vod/at/json",
      search: true
    }],
    movieg: [{
      name: "新马",
      url: "https://movie.gsl99.com/api.php/provide/vod/at/json",
      search: true
    }],
    zy018d: [{
      name: "十八",
      url: "https://www.zy018.com/api.php/provide/vod/at/json",
      search: true
    }],
    kuaiav: [{
      name: "快爱",
      url: "http://kuaiavzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    maozys: [{
      name: "猫咪",
      url: "https://api.maozyapi.com/inc/apijson_vod.php",
      search: true
    }],
    siwazy: [{
      name: "袜丝",
      url: "https://www.siwazyw.tv/api.php/provide/vod/at/json",
      search: true
    }],
    siwasd: [{
      name: "丝袜",
      url: "https://haiwai.siwazyw.org/api.php/provide/vod/at/json",
      search: true
    }],
    shayua: [{
      name: "鲨鱼",
      url: "https://shayuapi.com/api.php/provide/vod/",
      search: true
    }],
    apilyz: [{
      name: "老鸭",
      url: "https://api.apilyzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    semaod: [{
      name: "色猫",
      url: "https://caiji.semaozy.net/inc/apijson_vod.php",
      search: true
    }],
    putaoz: [{
      name: "葡萄",
      url: "https://caiji.putaozyw.net/inc/apijson_vod.php",
      search: true
    }],
    apidda: [{
      name: "滴滴",
      url: "https://api.ddapi.cc/api.php/provide/vod/at/json",
      search: true
    }],
    apixba: [{
      name: "️雪豹",
      url: "https://api.xbapi.cc/api.php/provide/vod/at/json",
      search: true
    }],
    timizy: [{
      name: "甜蜜",
      url: "https://timizy10.cc/api.php/provide/vod/at/json",
      search: true
    }],
    kkzydd: [{
      name: "写真",
      url: "https://kkzy.me/api.php/provide/vod/at/json",
      search: true
    }],
    madoud: [{
      name: "麻豆",
      url: "http://wuxu.cc/api.php/provide/vod/from/mdm3u8/",
      search: true
    }],
    tmyysd: [{
      name: "天美",
      url: "https://tmyy.cc/api.php/provide/vod/from/m3u8/",
      search: true
    }],
    apig14: [{
      name: "萝莉",
      url: "https://api.g14o.cc/api.php/provide/vod/at/json",
      search: true
    }],
    bominz: [{
      name: "博民",
      url: "https://www.bominzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    z82156: [{
      name: "至圣",
      url: "http://82.156.40.118:1234/api.php/provide/vod/at/json",
      search: true
    }],
    apizui: [{
      name: "最大",
      url: "https://api.zuidapi.com/api.php/provide/vod/at/json",
      search: true
    }],
    inmsdf: [{
      name: "映迷",
      url: "https://www.inmi.app/api.php/provide/vod/at/json",
      search: true
    }],
    apittz: [{
      name: "天天",
      url: "https://apittzy.com/api.php/provide/vod/at/json",
      search: true
    }],
    apilsb: [{
      name: "色逼",
      url: "https://apilsbzy1.com/api.php/provide/vod/at/json",
      search: true
    }],
    totose: [{
      name: "橘猫",
      url: "https://to.to-long.com/api.php/provide/vod/at/json",
      search: true
    }],
    hghhhd: [{
      name: "皇冠",
      url: "https://hghhh.com/api.php/provide/vod/at/json",
      search: true
    }],
    slapib: [{
      name: "森林",
      url: "https://slapibf.com/api.php/provide/vod/at/json",
      search: true
    }],
    lbapi9: [{
      name: "乐播",
      url: "https://lbapi9.com/api.php/provide/vod/at/json",
      search: true
    }],
    fhapi9: [{
      name: "番号",
      url: "http://fhapi9.com/api.php/provide/vod/at/json",
      search: true
    }]
  },
  appys: {
    jiagey: [{
      name: "飞鸽影视",
      url: "http://www.jiageys.top/api.php/app/",
      search: true
    }],
    futvds: [{
      name: "腐宅影视",
      url: "http://app.zhaifutvapp.com/ruifenglb_api.php/v1.vod",
      search: true
    }],
    mjlnld: [{
      name: "聚焦",
      url: "https://www.mjlnl.cn/api.php/v1.vod",
      search: true
    }],
    netfli: [{
      name: "奈飞狗",
      url: "https://netflixdog.club/api.php/app/",
      search: true
    }],
    yingsz: [{
      name: "鑫鑫影视",
      url: "http://yingszj.xn--654a.cc/api.php/app/",
      search: true
    }],
    ttmjas: [{
      name: "天天美剧",
      url: "https://www.ttmja.com/api.php/app/",
      search: true
    }],
    netfly: [{
      name: "奈飞",
      url: "https://www.netfly.tv/api.php/app/",
      search: true
    }],
    kuuses: [{
      name: "酷客",
      url: "https://www.kuin.one/api.php/app/",
      search: true
    }],
    siguyy: [{
      name: "思古影视",
      url: "https://siguyy.cc/api.php/app/",
      search: true
    }],
    umkasn: [{
      name: "UM影院",
      url: "https://www.umkan.cc/mogai_api.php/v1.vod",
      search: true
    }],
    kuindf: [{
      name: "北极狐",
      url: "https://www.kuin.one/api.php/app/",
      search: true
    }],
    zxxxxx: [{
      name: "种子短剧",
      url: "http://zzdj.cc/api.php/provide/vod",
      search: true
    }],
    weixia: [{
      name: "曲奇[V2]",
      url: "https://1234567.weixiaolove.cn/api.php/app/",
      search: true
    }],
    w996sd: [{
      name: "咕噜[V2]",
      url: "https://w.996w.top/api.php/app/",
      search: true
    }],
    t_7k78: [{
      name: "188影视",
      url: "https://www.7k789.com/mogai_api.php/v1.vod",
      search: true
    }],
    ystvdf: [{
      name: "追剧影视",
      url: "https://vip.ystv.lol/api.php/v1.vod",
      search: true
    }],
    bro51d: [{
      name: "零刻",
      url: "https://ys.51bro.cn/mogai_api.php/v1.vod",
      search: true
    }],
    hd90df: [{
      name: "爱城",
      url: "https://90hd.top/api.php/v1.vod",
      search: true
    }],
    qlkjyd: [{
      name: "奇乐",
      url: "http://qlkjy.cn/api.php/v1.vod",
      search: true
    }]
  },
  ikanbot: {
    url: "https://www.aikanbot.com"
  },
  yiso: {
    url: "https://yiso.fun",
    cookie: "__51vcke__JkIGvjjs25ETn0wz=28770cfe-3925-5532-9578-9952e161f2eb; __51vuft__JkIGvjjs25ETn0wz=1673341345700; satoken=207f3206-c936-4c4d-a0f1-36326eaa6498; __51uvsct__JkIGvjjs25ETn0wz=39; __vtins__JkIGvjjs25ETn0wz=%7B%22sid%22%3A%20%22e97aecc4-260c-5d9c-9799-622837656ad9%22%2C%20%22vd%22%3A%202%2C%20%22stt%22%3A%2024800%2C%20%22dr%22%3A%2024800%2C%20%22expires%22%3A%201711515404768%2C%20%22ct%22%3A%201711513604768%7D"
  },
  alist: [{
    name: "🐉神族九帝",
    server: "https://alist.shenzjd.com"
  }, {
    name: "💢repl",
    server: "https://ali.liucn.repl.co"
  }, {
    "name": "🌱正奕合集",
    "server": "http://www.jczyl.top:5244/"
  }, {
    "name": "🌱东哥",
    "server": "http://101.34.67.237:5244/"
  }, {
    "name": "🌱美云",
    "server": "https://h.dfjx.ltd/"
  }, {
    "name": "🌱小新盘",
    "server": "https://pan.cdnxin.top/"
  }, {
    "name": "🌱白云tv",
    "server": "http://breadmyth.asuscomm.com:22222/"
  }, {
    "name": "🌱小雅分类",
    "server": "http://www.214728327.xyz:5207/"
  }, {
    "name": "🌱瑶瑶",
    "server": "https://lyly.run.goorm.io/"
  }, {
    "name": "🌱潇洒个人",
    "server": "https://alist.azad.asia/"
  }, {
    "name": "🌱鹏程",
    "server": "https://pan.pengcheng.team/"
  }, {
    "name": "🌱小丫",
    "server": "http://alist.xiaoya.pro/"
  }, {
    "name": "🌱触光",
    "server": "https://pan.ichuguang.com"
  }, {
    "name": "🌱星梦",
    "server": "https://pan.bashroot.top"
  }, {
    "name": "🌱弱水分享",
    "server": "http://shicheng.wang:555/"
  }, {
    "name": "🌱神器云",
    "server": "https://alist.ygxz.xyz/"
  }, {
    "name": "🌱ecve资源",
    "server": "https://pan.ecve.cn/"
  }, {
    "name": "🌱雨呢",
    "server": "https://pan.clun.top/"
  }, {
    "name": "🌱OEIO",
    "server": "https://o.oeio.repl.co/"
  }, {
    "name": "🌱酷呵盘",
    "server": "https://pan.kuhehe.top/"
  }, {
    "name": "🌱分享者",
    "server": "https://melist.me/"
  }, {
    "name": "🌱目瞪口呆",
    "server": "https://pan.mdgd.cc/"
  }, {
    "name": "🌱小陈",
    "server": "https://ypan.cc/"
  }, {
    "name": "🌱动漫盘",
    "server": "http://pan.smjc.cc"
  }, {
    "name": "🌱神秘小盘",
    "server": "https://yun.ltt.zone/"
  }, {
    "name": "🌱神奇云",
    "server": "https://al.chirmyram.com/"
  }, {
    "name": "🌱SODAZ1",
    "server": "https://pan.sodaz.xyz/"
  }, {
    "name": "🌱ccaa",
    "server": "http://ww1.ccaac.xyz/"
  }, {
    "name": "🌱姹莱坞",
    "server": "https://alist.agczsz.top/",
    "passwd": "agree"
  }, {
    "name": "🌱杜比",
    "server": "https://dubi.tk"
  }, {
    "name": "🌱一只鱼",
    "server": "https://alist.youte.ml"
  }, {
    "name": "🌱神族九帝",
    "server": "https://alist.shenzjd.com"
  }, {
    "name": "🌱老谭",
    "server": "https://pan.cqtjy.cn/"
  }, {
    "name": "🌱皓星繁天",
    "server": "https://pan.hxft.xyz/"
  }, {
    "name": "🌱迅维云盘",
    "server": "https://pan.xwbeta.com"
  }, {
    "name": "🌱姬路白雪",
    "server": "https://pan.jlbx.xyz"
  }, {
    "name": "🌱肥灿",
    "server": "http://43.200.153.107:55609/"
  }, {
    "name": "🌱悦享盘",
    "server": "https://pity.eu.org/"
  }, {
    "name": "🌱小雅备用",
    "server": "http://120.76.118.109:5245/"
  }, {
    "name": "🌱小黄瓜",
    "server": "http://sbpan.tk/"
  }, {
    "name": "🌱梓凌妙妙",
    "server": "https://zi0.cc/"
  }, {
    "name": "👀微资·随意盘",
    "server": "https://apps.weixinqqq.com/"
  }, {
    "name": "🌱雨呢备用",
    "server": "https://clun.eu.org/"
  }, {
    "name": "🌱潇洒备用",
    "server": "http://www.azad.asia:5244/"
  }],
  color: [{
    light: {
      bg: "http://php.540734621.xyz/tcp/1.php",
      bgMask: "0x50ffffff",
      primary: "0xff446732",
      onPrimary: "0xffffffff",
      primaryContainer: "0xffc5efab",
      onPrimaryContainer: "0xff072100",
      secondary: "0xff55624c",
      onSecondary: "0xffffffff",
      secondaryContainer: "0xffd9e7cb",
      onSecondaryContainer: "0xff131f0d",
      tertiary: "0xff386666",
      onTertiary: "0xffffffff",
      tertiaryContainer: "0xffbbebec",
      onTertiaryContainer: "0xff002020",
      error: "0xffba1a1a",
      onError: "0xffffffff",
      errorContainer: "0xffffdad6",
      onErrorContainer: "0xff410002",
      background: "0xfff8faf0",
      onBackground: "0xff191d16",
      surface: "0xfff8faf0",
      onSurface: "0xff191d16",
      surfaceVariant: "0xffe0e4d6",
      onSurfaceVariant: "0xff191d16",
      inverseSurface: "0xff2e312b",
      inverseOnSurface: "0xfff0f2e7",
      outline: "0xff74796d",
      outlineVariant: "0xffc3c8bb",
      shadow: "0xff000000",
      scrim: "0xff000000",
      inversePrimary: "0xffaad291",
      surfaceTint: "0xff446732"
    },
    dark: {
      bg: "http://php.540734621.xyz/tcp/1.php",
      bgMask: "0x50000000",
      primary: "0xffaad291",
      onPrimary: "0xff173807",
      primaryContainer: "0xff2d4f1c",
      onPrimaryContainer: "0xffc5efab",
      secondary: "0xffbdcbb0",
      onSecondary: "0xff283420",
      secondaryContainer: "0xff3e4a35",
      onSecondaryContainer: "0xffd9e7cb",
      tertiary: "0xffa0cfcf",
      onTertiary: "0xff003738",
      tertiaryContainer: "0xff1e4e4e",
      onTertiaryContainer: "0xffbbebec",
      error: "0xffffb4ab",
      onError: "0xff690005",
      errorContainer: "0xff93000a",
      onErrorContainer: "0xffffdad6",
      background: "0xff11140e",
      onBackground: "0xffe1e4d9",
      surface: "0xff11140e",
      onSurface: "0xffe1e4d9",
      surfaceVariant: "0xff43483e",
      onSurfaceVariant: "0xffe1e4d9",
      inverseSurface: "0xffe1e4d9",
      inverseOnSurface: "0xff2e312b",
      outline: "0xff8d9286",
      outlineVariant: "0xff43483e",
      shadow: "0xff000000",
      scrim: "0xff000000",
      inversePrimary: "0xff446732",
      surfaceTint: "0xffaad291"
    }
  }, {
    light: {
      bg: "http://php.540734621.xyz/tcp/mb.php",
      bgMask: "0x50ffffff",
      primary: "0xff666014",
      onPrimary: "0xffffffff",
      primaryContainer: "0xffeee58c",
      onPrimaryContainer: "0xff1f1c00",
      secondary: "0xff625f42",
      onSecondary: "0xffffffff",
      secondaryContainer: "0xffe9e4be",
      onSecondaryContainer: "0xff1e1c05",
      tertiary: "0xff3f6654",
      onTertiary: "0xffffffff",
      tertiaryContainer: "0xffc1ecd5",
      onTertiaryContainer: "0xff002114",
      error: "0xffba1a1a",
      onError: "0xffffffff",
      errorContainer: "0xffffdad6",
      onErrorContainer: "0xff410002",
      background: "0xfffef9eb",
      onBackground: "0xff1d1c14",
      surface: "0xfffef9eb",
      onSurface: "0xff1d1c14",
      surfaceVariant: "0xffe7e3d0",
      onSurfaceVariant: "0xff1d1c14",
      inverseSurface: "0xff323128",
      inverseOnSurface: "0xfff5f1e3",
      outline: "0xff7a7768",
      outlineVariant: "0xffcbc7b5",
      shadow: "0xff000000",
      scrim: "0xff000000",
      inversePrimary: "0xffd1c973",
      surfaceTint: "0xff666014"
    },
    dark: {
      bg: "http://php.540734621.xyz/tcp/mb.php",
      bgMask: "0x50000000",
      primary: "0xffd1c973",
      onPrimary: "0xff353100",
      primaryContainer: "0xff4d4800",
      onPrimaryContainer: "0xffeee58c",
      secondary: "0xffcdc8a3",
      onSecondary: "0xff333117",
      secondaryContainer: "0xff4a482c",
      onSecondaryContainer: "0xffe9e4be",
      tertiary: "0xffa6d0b9",
      onTertiary: "0xff0e3727",
      tertiaryContainer: "0xff274e3d",
      onTertiaryContainer: "0xffc1ecd5",
      error: "0xffffb4ab",
      onError: "0xff690005",
      errorContainer: "0xff93000a",
      onErrorContainer: "0xffffdad6",
      background: "0xff14140c",
      onBackground: "0xffe7e2d5",
      surface: "0xff14140c",
      onSurface: "0xffe7e2d5",
      surfaceVariant: "0xff49473a",
      onSurfaceVariant: "0xffe7e2d5",
      inverseSurface: "0xffe7e2d5",
      inverseOnSurface: "0xff323128",
      outline: "0xff949181",
      outlineVariant: "0xff49473a",
      shadow: "0xff000000",
      scrim: "0xff000000",
      inversePrimary: "0xff666014",
      surfaceTint: "0xffd1c973"
    }
  }, {
    light: {
      bg: "http://php.540734621.xyz/tcp/pc.php",
      bgMask: "0x50ffffff",
      primary: "0xFF2B6C00",
      onPrimary: "0xFFFFFFFF",
      primaryContainer: "0xFFA6F779",
      onPrimaryContainer: "0xFF082100",
      secondary: "0xFF55624C",
      onSecondary: "0xFFFFFFFF",
      secondaryContainer: "0xFFD9E7CA",
      onSecondaryContainer: "0xFF131F0D",
      tertiary: "0xFF386666",
      onTertiary: "0xFFFFFFFF",
      tertiaryContainer: "0xFFBBEBEB",
      onTertiaryContainer: "0xFF002020",
      error: "0xFFBA1A1A",
      onError: "0xFFFFFFFF",
      errorContainer: "0xFFFFDAD6",
      onErrorContainer: "0xFF410002",
      background: "0xFFFDFDF5",
      onBackground: "0xFF1A1C18",
      surface: "0xFFFDFDF5",
      onSurface: "0xFF1A1C18",
      surfaceVariant: "0xFFE0E4D6",
      onSurfaceVariant: "0xFF1A1C18",
      inverseSurface: "0xFF2F312C",
      onInverseSurface: "0xFFF1F1EA",
      outline: "0xFF74796D",
      outlineVariant: "0xFFC3C8BB",
      shadow: "0xFF000000",
      scrim: "0xFF000000",
      inversePrimary: "0xFF8CDA60",
      surfaceTint: "0xFF2B6C00"
    },
    dark: {
      bg: "http://php.540734621.xyz/tcp/pc.php",
      bgMask: "0x50000000",
      primary: "0xFF8CDA60",
      onPrimary: "0xFF133800",
      primaryContainer: "0xFF1F5100",
      onPrimaryContainer: "0xFFA6F779",
      secondary: "0xFFBDCBAF",
      onSecondary: "0xFF283420",
      secondaryContainer: "0xFF3E4A35",
      onSecondaryContainer: "0xFFD9E7CA",
      tertiary: "0xFFA0CFCF",
      onTertiary: "0xFF003737",
      tertiaryContainer: "0xFF1E4E4E",
      onTertiaryContainer: "0xFFBBEBEB",
      error: "0xFFFFB4AB",
      errorContainer: "0xFF93000A",
      onError: "0xFF690005",
      onErrorContainer: "0xFFFFDAD6",
      background: "0xFF1A1C18",
      onBackground: "0xFFE3E3DC",
      outline: "0xFF8D9286",
      onInverseSurface: "0xFF1A1C18",
      inverseSurface: "0xFFE3E3DC",
      inversePrimary: "0xFF2B6C00",
      shadow: "0xFF000000",
      surfaceTint: "0xFF8CDA60",
      outlineVariant: "0xFF43483E",
      scrim: "0xFF000000",
      surface: "0xFF1A1C18",
      onSurface: "0xFFC7C7C0",
      surfaceVariant: "0xFF43483E",
      onSurfaceVariant: "0xFFC7C7C0"
    }
  }]
};
